package br.com.neppo.exceptions;

/**
 * Example error code structure class
 */
public enum ErrorCodes implements ErrorCode{

    // 4xx
    BAD_REQUEST("BAD_REQUEST", 400),
    NOT_SUPPORTED("NOT_SUPPORTED", 400),
    EMPTY("EMPTY", 400),
    EMPTY_FIELD("EMPTY_FIELD", 400),
    BAD_FORMATTING("BAD_FORMATTING", 400),
    INCORRECT_TYPE("INCORRECT_TYPE", 400),
    INVALID_VALUES("INVALID_VALUES", 400),
    FIELD_DOES_NOT_EXIST("FIELD_DOES_NOT_EXIST", 400),
    INVALID_FIELD("INVALID_FIELD", 400),
    INVALID_RANGE("INVALID_RANGE", 400),
    REPEATED_VALUES("REPEATED_VALUES", 400),
    ALREADY_ACTIVATED("ALREADY_ACTIVATED", 400),
    UNAUTHORIZED("UNAUTHORIZED", 401),
    PROHIBITED("PROHIBITED", 403),
    NOT_FOUND("NOT_FOUND", 404),
    METHOD_NOT_ALLOWED("METHOD_NOT_ALLOWED", 405),
    NOT_ACCEPTABLE("NOT_ACCEPTABLE", 406),
    TIME_OUT("TIME_OUT", 408),
    CONFLICT("CONFLICT", 409),
    ALREADY_EXISTS("ALREADY_EXISTS", 409),
    IGNORED("IM_A_TEAPOT", 418),


    // 5xx
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR", 500),
    FILE_NOT_SAVED("FILE_NOT_SAVED", 500),
    NOT_IMPLEMENTED("NOT_IMPLEMENTED", 501),
    NOT_AVAILABLE("NOT_AVAILABLE", 503),
    NOT_LOGGED_IN("NOT_LOGGED_IN", 530);

        private String code;
    private Integer status;

    ErrorCodes(String code, Integer status) {
        this.code = code;
        this.status = status;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public Integer getStatus() {
        return status;
    }

    @Override
    public String getName() {
        return this.name();
    }
}
