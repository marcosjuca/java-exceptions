package br.com.neppo.exceptions;

import br.com.neppo.exceptions.plugins.annotations.ExceptionPluginHandler;
import br.com.neppo.exceptions.plugins.interfaces.ExceptionHandler;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

import java.lang.reflect.Constructor;
import java.util.*;

public class ExceptionProcessor {

    private ExceptionProcessor(){}

    // map of priority + list of handlers
    private static Map<Integer, List<ExceptionHandler>> priorityMap;

    private static boolean loaded = false;

    public static void load(){
        try{
            if(!loaded){
                scanAndLoad();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("Error while working with plugins", e);
        }
    }

    /**
     * Scans br.com.neppo.ian.exception.plugins.plugs
     * and looks for classes that implements Handler and is annotated with @PluginHandler
     * @throws Exception
     */
    private static void scanAndLoad() throws Exception{

        // find all classes annoated with @PluginHandler in package plugs

        FastClasspathScanner scanner = new FastClasspathScanner();
        ScanResult result = scanner.scan();

        List<Class<?>> annotated = stringsToClasses(result.getNamesOfClassesWithAnnotation(ExceptionPluginHandler.class));

        // avoiding those pesky NullPointerErrors
        if(annotated == null){
            loaded = true;
            return;
        }

        // Cycles through the classes, create an instance of them and add them to the array
        for (Class<?> handler : annotated){

            if(!ExceptionHandler.class.isAssignableFrom(handler)){
                throw new Exception("Custom error does not implement the Handler interface.");
            }

            // Gets priority of the class
            Integer priority = handler.getAnnotation(ExceptionPluginHandler.class).priority();

            // Creates an instance
            Constructor<?> constructor = handler.getConstructor();
            ExceptionHandler object = (ExceptionHandler) constructor.newInstance();

            // adds to the current map.
            addToMap(priority, object);
        }
        loaded = true;
    }

    /**
     * Adds the handler to the given priority.
     * @param priority priority; smaller means less urgent
     * @param handler object that can handle the task.
     */
    private static void addToMap(Integer priority, ExceptionHandler handler){

        // creates instance of hashmap if necessary
        if(priorityMap == null){
            priorityMap = new HashMap<>();
        }

        // adds
        if(priorityMap.get(priority) == null){
            priorityMap.put(priority, new ArrayList<>());
        }

        priorityMap.get(priority).add(handler);
    }

    /**
     * converts a list of classname to a list of classes
     * @param strings class names
     * @return list of classes
     */
    private static List<Class<?>> stringsToClasses(List<String> strings){

        List<Class<?>> classes = new ArrayList<>();

        for (String s:strings) {
            try{
                classes.add(Class.forName(s));
            }
            catch (ClassNotFoundException e){
                e.printStackTrace();
            }
        }

        return classes;
    }

    /**
     * Tries to convert an exception -- otherwise return null
     * @param throwable
     * @return
     */
    public static DynamicException process(Throwable throwable){

        if(priorityMap == null){
            return null;
        }

        // gets the list of priorities and orders them
        List<Integer> keys = new ArrayList<>(priorityMap.keySet());
        Collections.sort(keys);     // sorts them in order
        Collections.reverse(keys);  // biggest first


        // cycles through the items and converts
        for(Integer key : keys){

            List<ExceptionHandler> handlers = priorityMap.get(key);

            for (ExceptionHandler handler : handlers) {

                DynamicException result = handler.convert(throwable);

                if(result != null){
                    return  result;
                }
            }
        }

        return null;
    }
}
