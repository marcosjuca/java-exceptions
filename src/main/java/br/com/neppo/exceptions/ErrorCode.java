package br.com.neppo.exceptions;

public interface ErrorCode {
    String getCode();
    Integer getStatus();
    String getName();
}
