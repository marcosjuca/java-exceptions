package br.com.neppo.exceptions.plugins.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExceptionPluginHandler {

    int priority() default 0;
    Class<?> value() default Object.class;
}
