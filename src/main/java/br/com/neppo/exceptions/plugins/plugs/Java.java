package br.com.neppo.exceptions.plugins.plugs;

import br.com.neppo.exceptions.DynamicException;
import br.com.neppo.exceptions.ErrorCodes;
import br.com.neppo.exceptions.plugins.annotations.ExceptionPluginHandler;
import br.com.neppo.exceptions.plugins.interfaces.ExceptionHandler;

@ExceptionPluginHandler(Java.class)
public class Java implements ExceptionHandler {

    @Override
    public DynamicException convert(Throwable t) {

        // masking the nullpointer exceptions
        if(ExceptionHandler.instanceOf(t, NullPointerException.class)){
            return DynamicException.toDynamicException("Houve um erro ao executar a operacao").errorCode(ErrorCodes.INTERNAL_SERVER_ERROR);
        }

        return null;
    }

}
