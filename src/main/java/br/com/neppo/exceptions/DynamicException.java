package br.com.neppo.exceptions;

import br.com.neppo.jlibs.CustomResponse;
import br.com.neppo.jlibs.bundles.BundleHandler;
import org.apache.commons.lang3.StringUtils;

public class DynamicException extends RuntimeException {

    private String code;
    private Integer status;
    private String field;
    private String message;
    private String[] placeholdersOfMessage;

    public DynamicException(){
        super();
    }

    private void baseError(){
        this.code = "INTERNAL_SERVER_ERROR";
        this.status = 500;
    }

    public DynamicException(String message){
        super(message);
        this.baseError();
    }

    public DynamicException(String message, Throwable throwable){
        super(message, throwable);
        this.baseError();
    }

    public DynamicException(ErrorCode code, String message){
        super(message);
        this.setFromCode(code);
    }

    public DynamicException(ErrorCode code, String message, Throwable throwable){
        super(message, throwable);
        this.setFromCode(code);
    }

    public DynamicException(ErrorCode code, String field, String message){
        super(message);
        this.setFromCode(code);
        this.setField(field);
    }

    public DynamicException(ErrorCode code, String field, String message, Throwable throwable){
        super(message, throwable);
        this.setFromCode(code);
        this.setField(field);
    }

    public void setFromCode(ErrorCode code){
        this.code = code.getCode();
        this.status = code.getStatus();
    }

    public String getCode() {
        return code;
    }

    public Integer getStatus() {
        return status;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage(){
        if(StringUtils.isEmpty(this.message)){
            return super.getMessage();
        }
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String[] getPlaceholdersOfMessage() {
        return placeholdersOfMessage;
    }

    public void setPlaceholdersOfMessage(String[] placeholdersOfMessage) {
        this.placeholdersOfMessage = placeholdersOfMessage;
    }

    public DynamicException code(String code){
        this.code = code;
        return this;
    }

    public DynamicException field(String field){
        this.field = field;
        return this;
    }

    public DynamicException status(Integer status){
        this.status = status;
        return this;
    }

    public DynamicException errorCode(ErrorCode codes){
        this.setFromCode(codes);
        return this;
    }

    public DynamicException message(String message){
        this.message = message;
        return this;
    }

    public DynamicException placeholdersOfMessage(String[] placeholdersOfMessage){
        this.placeholdersOfMessage = placeholdersOfMessage;
        return this;
    }

    public static DynamicException toDynamicException(Throwable throwable, ErrorCode code){
        if(isDynamicException(throwable)){
            return (DynamicException) throwable;
        }

        DynamicException output = new DynamicException(throwable.getMessage(), throwable.getCause());

        if(code != null){
            output.status(code.getStatus())
                    .code(code.getCode());
        }
        output.setFromCode(code != null ? code : ErrorCodes.INTERNAL_SERVER_ERROR);

        return output;
    }

    public static DynamicException toDynamicException(Throwable throwable){
        return toDynamicException(throwable, null);
    }

    public static DynamicException toDynamicException(String message, ErrorCode code){
        return toDynamicException(new Exception(message), code);
    }

    public static DynamicException toDynamicException(String message){
        return toDynamicException(new Exception(message));
    }

    public static DynamicException toDynamicExceptionWithPlugin(Throwable t){

        if(t instanceof DynamicException){
            return (DynamicException) t;
        }

        DynamicException converted = ExceptionProcessor.process(t);

        if(converted == null){
            return toDynamicException(t);
        }

        return converted;

    }

    public static CustomResponse<Object> toCustomResponse(DynamicException exception){

        CustomResponse<Object> customResponse = new CustomResponse<>();
        customResponse.setData(null);
        customResponse.setCode(exception.getCode());
        customResponse.setStatus(exception.getStatus());
        customResponse.setMessage(BundleHandler.resolveMessage(exception.getStatus(), exception.getCode(), exception.getField(), exception.getMessage(), exception.getPlaceholdersOfMessage()));
        customResponse.setField(exception.getField());
        return customResponse;
    }

    public static CustomResponse<Object> toCustomResponse(Exception e, ErrorCode code){
        if(code != null){
            return toCustomResponse(toDynamicException(e, code));
        }
        return toCustomResponse(toDynamicException(e));
    }

    public static CustomResponse<Object> toCustomResponse(String message, ErrorCode code){
        if(code != null){
            return toCustomResponse(toDynamicException(message, code));
        }
        return toCustomResponse(toDynamicException(message));
    }

    public static boolean isDynamicException(Throwable throwable){
        return throwable instanceof DynamicException;
    }

    /**
     * resolves throwable messages
     * @param e
     * @return
     */
    public static String resolveMessage(Throwable e){
        DynamicException exception = toDynamicException(e);
        return BundleHandler.resolveMessage(
                exception.getStatus(),
                exception.getCode(),
                exception.getField(),
                exception.getMessage());
    }

    public <T> CustomResponse<T> applyTo(CustomResponse<T> resp){
        if(resp == null){
            return null;
        }
        String message = resolveMessage(this);
        resp.setMessage(message);
        resp.setCode(code);
        resp.setStatus(status);

        return resp;
    }

    public CustomResponse toCustomResponse(){
        return applyTo(new CustomResponse<>());
    }
}
